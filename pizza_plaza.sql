-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 25 2021 г., 11:38
-- Версия сервера: 5.7.33
-- Версия PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pizza_plaza`
--

-- --------------------------------------------------------

--
-- Структура таблицы `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `streetnumber` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `customer`
--

INSERT INTO `customer` (`id`, `firstname`, `lastname`, `street`, `streetnumber`, `zip`, `city`, `phone`) VALUES
(1, 'qweqw', 'qeqw', 'wqeqw', 'wqeqw', 'qwewe', 'wqeqwe', 'qwe'),
(2, 'qweqw', 'qeqw', 'wqeqw', 'wqeqw', 'qwewe', 'wqeqwe', 'qwe'),
(4, 'qweqw', 'qeqw', 'wqeqw', 'wqeqw', 'qwewe', 'wqeqwe', 'qwe'),
(5, 'qweqw', 'qeqw', 'wqeqw', 'wqeqw', 'qwewe', 'wqeqwe', 'qwe');

-- --------------------------------------------------------

--
-- Структура таблицы `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210820135809', '2021-08-20 16:58:30', 227),
('DoctrineMigrations\\Version20210820140432', '2021-08-20 17:04:40', 139),
('DoctrineMigrations\\Version20210824144601', '2021-08-24 17:46:13', 574);

-- --------------------------------------------------------

--
-- Структура таблицы `extras`
--

CREATE TABLE `extras` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `is_choosable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `extras`
--

INSERT INTO `extras` (`id`, `name`, `price`, `is_choosable`) VALUES
(1, 'Tomaten', 1, 0),
(2, 'Käse', 1, 0),
(3, 'Sardellen', 1, 1),
(4, 'Oliven', 1, 1),
(5, 'Salami', 0.5, 1),
(6, 'Schinken', 0.5, 1),
(7, 'Champignons', 0.75, 1),
(8, 'Paprika', 0.5, 1),
(9, 'Mozzarella', 1, 1),
(10, 'Basilikum', 0.75, 1),
(11, 'Peperoniwurst', 1, 1),
(12, 'Zwiebeln', 0.5, 1),
(13, 'Knoblauch', 0.5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `timestamp`, `customer_id`) VALUES
(2, '2021-08-24 19:49:28', 4),
(3, '2021-08-24 19:50:18', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `pizzas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `order_items`
--

INSERT INTO `order_items` (`id`, `quantity`, `order_id`, `pizzas_id`) VALUES
(4, 1, 2, 5),
(5, 1, 2, 5),
(6, 1, 2, 5),
(7, 1, 3, 5),
(8, 1, 3, 5),
(9, 1, 3, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `order_item_has_extra`
--

CREATE TABLE `order_item_has_extra` (
  `id` int(11) NOT NULL,
  `order_items_id` int(11) NOT NULL,
  `extras_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `order_item_has_extra`
--

INSERT INTO `order_item_has_extra` (`id`, `order_items_id`, `extras_id`) VALUES
(4, 4, 3),
(5, 5, 3),
(6, 5, 4),
(7, 7, 3),
(8, 8, 3),
(9, 8, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `pizzas`
--

CREATE TABLE `pizzas` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pizzas`
--

INSERT INTO `pizzas` (`id`, `name`, `price`, `description`) VALUES
(4, 'Magherita', 4, 'Eine leckere Käsepizza.'),
(5, 'Napoli', 5.5, 'Ein Mix aus Sardellen und Oliven bieten den ultimativen Flair.'),
(6, 'Salami', 4.7, 'Der Klassiker unter den Pizzen.'),
(7, 'Prosciutto', 4.7, 'Der im Volksmund auch als Schinkenpizza bekannte Klassiker.'),
(8, 'Funghi', 4.7, 'Die beste Wahl für alle Pilzliebhaber.'),
(9, 'Salami-Paprika', 5.5, 'Die beste Kombination zwischen deftig und frisch!'),
(10, 'Tricolore', 5.5, '100% vegetarisch - 100% lecker!'),
(11, 'Diavolo', 5.5, 'Teuflisch scharf!'),
(12, 'Roma', 5.5, 'Schinken und Champignons, köstlich im Steinofen zubereitet.');

-- --------------------------------------------------------

--
-- Структура таблицы `pizza_has_extra`
--

CREATE TABLE `pizza_has_extra` (
  `id` int(11) NOT NULL,
  `pizzas_id` int(11) NOT NULL,
  `extras_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pizza_has_extra`
--

INSERT INTO `pizza_has_extra` (`id`, `pizzas_id`, `extras_id`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 5, 1),
(4, 5, 2),
(5, 5, 3),
(6, 5, 4),
(7, 6, 1),
(8, 6, 2),
(9, 6, 5),
(10, 7, 1),
(11, 7, 2),
(12, 7, 6),
(13, 8, 1),
(14, 8, 2),
(15, 8, 7),
(16, 9, 1),
(17, 9, 2),
(18, 9, 5),
(19, 9, 8),
(20, 10, 1),
(21, 10, 2),
(22, 10, 9),
(23, 10, 10),
(24, 11, 1),
(25, 11, 2),
(26, 11, 11),
(27, 11, 12),
(28, 11, 13),
(29, 12, 1),
(30, 12, 2),
(31, 12, 11),
(32, 12, 12),
(33, 12, 13);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_item_has_extra`
--
ALTER TABLE `order_item_has_extra`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pizzas`
--
ALTER TABLE `pizzas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `extras`
--
ALTER TABLE `extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `order_item_has_extra`
--
ALTER TABLE `order_item_has_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `pizzas`
--
ALTER TABLE `pizzas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
