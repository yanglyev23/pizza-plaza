<?php
namespace  App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdminService
{
    private $password;
    public function __construct(ParameterBagInterface $params)
    {
        $this -> password = $params -> get('admin.password');
    }
    public function checkPassword($enteredPassword)
    {
        if(password_verify($enteredPassword, $this -> password))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}