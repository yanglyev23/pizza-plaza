<?php

namespace App\Entity;

use App\Repository\OrderItemsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemsRepository::class)
 */
class OrderItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $Order_ID;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pizzas_ID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getOrderID(): ?int
    {
        return $this->Order_ID;
    }

    public function setOrderID(int $Order_ID): self
    {
        $this->Order_ID = $Order_ID;

        return $this;
    }

    public function getPizzasID(): ?int
    {
        return $this->Pizzas_ID;
    }

    public function setPizzasID(int $Pizzas_ID): self
    {
        $this->Pizzas_ID = $Pizzas_ID;

        return $this;
    }
}
