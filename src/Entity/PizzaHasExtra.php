<?php

namespace App\Entity;

use App\Repository\PizzaHasExtraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PizzaHasExtraRepository::class)
 */
class PizzaHasExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pizzas_ID;

    /**
     * @ORM\Column(type="integer")
     */
    private $Extras_ID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPizzasID(): ?int
    {
        return $this->Pizzas_ID;
    }

    public function setPizzasID(int $Pizzas_ID): self
    {
        $this->Pizzas_ID = $Pizzas_ID;

        return $this;
    }

    public function getExtrasID(): ?int
    {
        return $this->Extras_ID;
    }

    public function setExtrasID(int $Extras_ID): self
    {
        $this->Extras_ID = $Extras_ID;

        return $this;
    }
}
