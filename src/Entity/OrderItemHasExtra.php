<?php

namespace App\Entity;

use App\Repository\OrderItemHasExtraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemHasExtraRepository::class)
 */
class OrderItemHasExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $OrderItems_ID;

    /**
     * @ORM\Column(type="integer")
     */
    private $Extras_ID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderItemsID(): ?int
    {
        return $this->OrderItems_ID;
    }

    public function setOrderItemsID(int $OrderItems_ID): self
    {
        $this->OrderItems_ID = $OrderItems_ID;

        return $this;
    }

    public function getExtrasID(): ?int
    {
        return $this->Extras_ID;
    }

    public function setExtrasID(int $Extras_ID): self
    {
        $this->Extras_ID = $Extras_ID;

        return $this;
    }
}
