<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\Driver\Connection;
use App\Entity\Pizzas;
use App\Entity\Extras;
use App\Entity\PizzaHasExtra;
use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderItems;
use App\Entity\OrderItemHasExtra;
use App\Service\AdminService;

class AdminController extends AbstractController
{
    public function specialOffer()
	{
        $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
        $pizzas = $entityManager->findAll();
        $pizzasID = [];
        foreach($pizzas as $pizza){
            array_push($pizzasID, $pizza->getId());
		}
        srand(date("ymd"));
        $offerID = rand(min($pizzasID), max($pizzasID));
        $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
        $offer = $entityManager->find($offerID);
        return $offer;
    }
    public function getOrders()
	{
        $entityManager = $this->getDoctrine()->getRepository(Customer::class);
        $customers = $entityManager->findAll();
        $customersResult = [];
        foreach($customers as $customer)
        {
            $customers_ = [];
            $customers_ += array('id' => $customer->getId());
            $customers_ += array('firstname' => $customer->getFirstname());
            $customers_ += array('lastname' => $customer->getLastname());
            $customers_ += array('street' => $customer->getStreet());
            $customers_ += array('streetnumber' => $customer->getStreetnumber());
            $customers_ += array('zip' => $customer->getZip());
            $customers_ += array('city' => $customer->getCity());
            $customers_ += array('phone' => $customer->getPhone());

            $customerID = $customer->getId();
            $entityManager = $this->getDoctrine()->getRepository(Order::class);

            $orders = $entityManager->findBy(['Customer_ID' => $customerID]);
            $ordersResult = [];
            foreach($orders as $order)
            {
                $orders_ = [];
                $orders_ += array('id' => $order->getId());
                $orders_ += array('timestamp' => $order->getTimestamp());
                
                $orderID = $order->getId();
                $entityManager = $this->getDoctrine()->getRepository(OrderItems::class);
                $orderItems = $entityManager->findBy(['Order_ID' => $orderID]);
                $orderItemsResult = [];
                foreach($orderItems as $orderItem)
                {
                    $orderItems_ = [];
                    $orderItems_ += array('id' => $orderItem->getId());
                    $orderItems_ += array('quantity' => $orderItem->getQuantity());
                    $orderItems_ += array('pizzaid' => $orderItem->getPizzasID());
                    $pizzaID = $orderItem->getPizzasID();
                    $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
                    $pizza = $entityManager->findBy(['id' => $pizzaID]);
                    $orderItems_ += array('pizzaname' => $pizza[0]->getName());
                    $orderItems_ += array('pizzaprice' => $pizza[0]->getPrice());
                    $orderItems_ += array('pizzadescription' => $pizza[0]->getDescription());
                    $orderItemID = $orderItem->getId();
                    $entityManager = $this->getDoctrine()->getRepository(OrderItemHasExtra::class);
                    $orderItemHasExtra = $entityManager->findBy(['OrderItems_ID' => $orderItemID]);
                    $extrasID = array();
                    foreach($orderItemHasExtra as $extra){
                        array_push($extrasID, $extra->getExtrasID());
                    }
                    $entityManager = $this->getDoctrine()->getRepository(Extras::class);
                    $extras = $entityManager->findBy(array('id' => $extrasID));
                    $extrasResult = array();
                    foreach ($extras as $extra) {
                        if($extra->getIsChoosable() == 1){
                            $extra_ = array();
                            $extra_ += array('id' => $extra->getId());
                            $extra_ += array('name' => $extra->getName());
                            $extra_ += array('price' => $extra->getPrice());
                            array_push($extrasResult, $extra_);
                        }
                    }
                    $orderItems_ += array('extras' => $extrasResult);
                    array_push($orderItemsResult, $orderItems_);
                }
                $orders_ += array('orderItems' => $orderItemsResult);
                array_push($ordersResult, $orders_);
            }
            $customers_ += array('orders' => $ordersResult);
            array_push($customersResult, $customers_);
        }
        return $customersResult;
    }
    
    public function adminPanel() : Response
    {
        return $this->render('adminEnter.html.twig', array(
            'page' => ''
        ));
    }
    public function deleteOrder() : Response
    {
        $CustomerID = $_POST['delete'];
        $entityManager = $this->getDoctrine()->getRepository(Customer::class);
        $customers = $entityManager->find($CustomerID);


        $entityManager = $this->getDoctrine()->getRepository(Order::class);
        $orders = $entityManager->findBy([
			'Customer_ID' => $customers->getId(),
		]);

		$ordersID = array();
		foreach($orders as $order){
			array_push($ordersID, $order->getId());
		}
        $entityManager = $this->getDoctrine()->getRepository(OrderItems::class);
		$orderItems = $entityManager->findBy(array('Order_ID' => $ordersID));
		$orderItemsID = array();

		foreach($orderItems as $orderItem){
			array_push($orderItemsID, $orderItem->getId());
		}
        $entityManager = $this->getDoctrine()->getRepository(OrderItemHasExtra::class);
		$orderItemHasExtras = $entityManager->findBy(array('OrderItems_ID' => $orderItemsID));
		$entityManager = $this->getDoctrine()->getManager();

	    $entityManager->remove($customers);
	    foreach($orders as $order){
	    	$entityManager->remove($order);
	    }
	    foreach($orderItems as $orderItem){
	    	$entityManager->remove($orderItem);
	    }
	    foreach($orderItemHasExtras as $orderItemHasExtra){
	    	$entityManager->remove($orderItemHasExtra);
	    }
	    $entityManager->flush();
        return $this->render('success.html.twig', array(
            'page' => '',
            'message' => 'Bestellung erfolgreich bearbeitet'
        ));
    }
    public function adminEnter(AdminService $adminService) : Response
    {
        $password = $_POST['password'];
        $result = $adminService -> checkPassword($password);
        if ($result)
        {
            $offer = $this -> specialOffer();
            $orders = $this -> getOrders();
            return $this->render('orders.html.twig', array(
                'page' => '',
                'customers' => $orders,
                'offer' => $offer
            ));
        }
        else
        {
            return $this->render('success.html.twig', array(
                'page' => '',
                'message' => 'Passwort falsch eingegeben'
            ));
        }
    }
}