<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\Driver\Connection;
use App\Entity\Pizzas;
use App\Entity\Extras;
use App\Entity\PizzaHasExtra;
use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderItems;
use App\Entity\OrderItemHasExtra;


class MainController extends AbstractController
{
    public function index() : Response
	{
        $offer = $this -> specialOffer();
        return $this->render('index.html.twig', array(
            'page' => 'index',
            'offer' => $offer
        ));
    }
    public function About() : Response
	{
        return $this->render('about.html.twig', array(
            'page' => 'about'
        ));
    }
    public function Contact() : Response
	{
        return $this->render('contact.html.twig', array(
            'page' => 'contact'
        ));
    }
    public function Imprint()
	{
        return $this->render('imprint.html.twig', array(
            'page' => 'imprint'
        ));
    }
    public function getPizzas()
	{
        $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
        $pizzas = $entityManager->findAll();
        $pizzasResult = [];
        foreach($pizzas as $pizza)
        {
            $pizzas_ = [];
            $pizzas_ += array('id' => $pizza->getId());
			$pizzas_ += array('name' => $pizza->getName());
			$pizzas_ += array('price' => $pizza->getPrice());
            $pizzas_ += array('description' => $pizza->getDescription());

            $pizzaID = $pizza->getId();
			$entityManager = $this->getDoctrine()->getRepository(PizzaHasExtra::class);
            $pizzaHasExtra = $entityManager->findBy(['Pizzas_ID' => $pizzaID]);

            $extrasID = array();
			foreach($pizzaHasExtra as $extra){
				array_push($extrasID, $extra->getExtrasID());
			}
            $entityManager = $this->getDoctrine()->getRepository(Extras::class);
            $extras = $entityManager->findBy(array('id' => $extrasID));
			$extrasResult = array();
			foreach ($extras as $extra) {
				if($extra->getIsChoosable() == 1){
					$extra_ = array();
					$extra_ += array('id' => $extra->getId());
					$extra_ += array('name' => $extra->getName());
					$extra_ += array('price' => $extra->getPrice());
					array_push($extrasResult, $extra_);
				}

			}
            $pizzas_ += array('extras' => $extrasResult);
            array_push($pizzasResult, $pizzas_);
        }
        
        return $pizzasResult;
    }
    public function onlineOrder() : Response
	{
        $offer = $this -> specialOffer();
        $pizzas = $this -> getPizzas();
        return $this->render('onlineOrder.html.twig', array(
            'page' => 'onlineOrder',
            'pizzas' => $pizzas,
            'offer' => $offer
        ));
    }
    public function arrangeOrder() : Response
	{
        $pizzas = $this -> getPizzas();
        return $this->render('arrangeOrder.html.twig', array(
            'page' => '',
            'pizzas' => $pizzas
        ));
    }
    public function specialOffer()
	{
        $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
        $pizzas = $entityManager->findAll();
        $pizzasID = [];
        foreach($pizzas as $pizza){
            array_push($pizzasID, $pizza->getId());
		}
        srand(date("ymd"));
        $offerID = rand(min($pizzasID), max($pizzasID));
        $entityManager = $this->getDoctrine()->getRepository(Pizzas::class);
        $offer = $entityManager->find($offerID);
        return $offer;
    }
    public function submitOrder()
	{
        if (isset($_POST['name'])) $name = $_POST['name'];
        if (isset($_POST['surname'])) $surname = $_POST['surname'];
        if (isset($_POST['street'])) $street = $_POST['street'];
        if (isset($_POST['house'])) $house = $_POST['house'];
        if (isset($_POST['zip'])) $zip = $_POST['zip'];
        if (isset($_POST['city'])) $city = $_POST['city'];
        if (isset($_POST['phone'])) $phone = $_POST['phone'];

        $entityManager = $this->getDoctrine()->getManager();

        $customer = new Customer();
        $customer-> setFirstname($name);
        $customer-> setLastname($surname);
        $customer-> setStreet($street);
        $customer-> setStreetnumber($house);
        $customer-> setZip($zip);
        $customer-> setCity($city);
        $customer-> setPhone($phone);

        $entityManager->persist($customer);
        $entityManager->flush();
        
        $order = new Order();
        $order->setTimestamp(new \DateTime());
        $order->setCustomerID($customer->getId());

        $entityManager->persist($order);
        $entityManager->flush();
        
        if(!empty($_POST['order']))
        {
            foreach($_POST['order'] as $id => $data)
            {
                $orderItems = new OrderItems();
                $orderItems->setQuantity(intval($data["quantity"]));
                $orderItems->setOrderID($order->getId());
                $orderItems->setPizzasID(intval($data["pizzaID"]));
               
                $entityManager->persist($orderItems);
                $entityManager->flush();

                $extras = explode('extra',$data["extrasID"]);
                for ($i=1; $i<count($extras); $i++)
                {
                    if (!is_null($extras[$i]))
                    {
                        $orderItemHasExtra = new OrderItemHasExtra();   
                        $orderItemHasExtra-> setOrderItemsID($orderItems->getId());
                        $orderItemHasExtra-> setExtrasID(intval($extras[$i]));

                        $entityManager->persist($orderItemHasExtra);
                        $entityManager->flush();
                    }
                }
            }
        }
        return $this->render('success.html.twig', array(
            'page' => '',
            'message' => 'Bestellung wurde erfolgreich abgeschlossen'
        ));
    }
}